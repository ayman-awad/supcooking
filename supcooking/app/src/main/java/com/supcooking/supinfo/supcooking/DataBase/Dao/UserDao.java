package com.supcooking.supinfo.supcooking.DataBase.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.supcooking.supinfo.supcooking.Classes.User;

@Dao
public interface UserDao {

    @Query("SELECT * FROM users")
    User getAll();

    @Query("SELECT * FROM users WHERE id_user=:id")
    User getUserById(long id);

    @Query("SELECT id_user FROM users WHERE username=:username AND password=:password")
    long userExist(String username,String password);


    @Insert(onConflict = OnConflictStrategy.ABORT)
    long insert(User  user);

    @Delete
    void delete(User...  user);

    @Update
    void update(User...  user);
}
