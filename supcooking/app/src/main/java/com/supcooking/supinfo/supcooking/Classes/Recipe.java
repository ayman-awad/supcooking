package com.supcooking.supinfo.supcooking.Classes;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "recipes",
        foreignKeys = @ForeignKey(entity = User.class,
                parentColumns = "id_user",
                childColumns = "fk_id_user",
                onDelete = CASCADE),
        indices= @Index(value="fk_id_user"))

public class Recipe {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_recipe")
    private long idRecipe;

    @ColumnInfo(name = "name_recipe")
    private String name;

    @ColumnInfo(name = "type")
    private String type;

    @ColumnInfo(name = "time_cooking")
    private int timeCooking;

    @ColumnInfo(name = "time_preparation")
    private int timePreparation;

    @ColumnInfo(name = "steps_of_preparation")
    private String stepsPreparation;

    @ColumnInfo(name = "ingredient")
    private String ingredient;

    @ColumnInfo(name = "url_image")
    private String pathImg;

    @ColumnInfo(name = "fk_id_user")
    private long userId;


    public Recipe(){super(); }

    public Recipe(String nameRecipe,
                       String typeRecipe,
                       int timeOfCookingRecipe,
                       int timeOfPreparationRecipe,
                       String stepPreparationRecipe,
                       String ingredient,
                       String pathImgRecipe,
                       long fkUserId) {

        this.name = nameRecipe;
        this.type = typeRecipe;
        this.timeCooking = timeOfCookingRecipe;
        this.timePreparation = timeOfPreparationRecipe;
        this.stepsPreparation = stepPreparationRecipe;
        this.setIngredient(ingredient);
        this.pathImg = pathImgRecipe;
        this.userId = fkUserId;
    }

    @NonNull
    public long getIdRecipe() {
        return idRecipe;
    }

    public void setIdRecipe(@NonNull long idRecipe) {
        this.idRecipe = idRecipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTimeCooking() {
        return timeCooking;
    }

    public void setTimeCooking(int timeCooking) {
        this.timeCooking = timeCooking;
    }

    public int getTimePreparation() {
        return timePreparation;
    }

    public void setTimePreparation(int timePreparation) {
        this.timePreparation = timePreparation;
    }

    public String getStepsPreparation() {
        return stepsPreparation;
    }

    public void setStepsPreparation(String stepsPreparation) {
        this.stepsPreparation = stepsPreparation;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public String getPathImg() {
        return pathImg;
    }

    public void setPathImg(String pathImg) {
        this.pathImg = pathImg;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return getName() +" "+getType();
    }


}
