package com.supcooking.supinfo.supcooking.Classes;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "recipeFromApi")
public class RecipeFromApi {

    @NonNull
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id_recipe")
    private long idRecipe;

    @ColumnInfo(name = "name_recipe")
    private String name;

    @ColumnInfo(name = "type")
    private String type;

    @ColumnInfo(name = "time_cooking")
    private float timeCooking;

    @ColumnInfo(name = "time_preparation")
    private float timePreparation;

    @ColumnInfo(name = "steps_of_preparation")
    private String stepsPreparation;

    @ColumnInfo(name = "ingredient")
    private String ingredient;

    @ColumnInfo(name = "url_image")
    private String pathImg;

    public RecipeFromApi(){
        super();
    }

    public RecipeFromApi(String nameRecipe,
                         String typeRecipe,
                         float timeOfCookingRecipe,
                         float timeOfPreparationRecipe,
                         String stepPreparationRecipe,
                         String ingredient,
                         String pathImgRecipe) {

        this.setName(nameRecipe);
        this.setType(typeRecipe);
        this.setTimeCooking(timeOfCookingRecipe);
        this.setTimePreparation(timeOfPreparationRecipe);
        this.setStepsPreparation(stepPreparationRecipe);
        this.setIngredient(ingredient);
        this.setPathImg(pathImgRecipe);
    }

    @NonNull
    public long getIdRecipe() {
        return idRecipe;
    }

    public void setIdRecipe(@NonNull long idRecipe) {
        this.idRecipe = idRecipe;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getTimeCooking() {
        return timeCooking;
    }

    public void setTimeCooking(float timeCooking) {
        this.timeCooking = timeCooking;
    }

    public float getTimePreparation() {
        return timePreparation;
    }

    public void setTimePreparation(float timePreparation) {
        this.timePreparation = timePreparation;
    }

    public String getStepsPreparation() {
        return stepsPreparation;
    }

    public void setStepsPreparation(String stepsPreparation) {
        this.stepsPreparation = stepsPreparation;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public String getPathImg() {
        return pathImg;
    }

    public void setPathImg(String pathImg) {
        this.pathImg = pathImg;
    }

    public String toString() {
        return getName();
    }
}
