package com.supcooking.supinfo.supcooking.Activity.user;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.supcooking.supinfo.supcooking.Activity.LoginActivity;
import com.supcooking.supinfo.supcooking.Classes.User;
import com.supcooking.supinfo.supcooking.DataBase.MyDataBase;
import com.supcooking.supinfo.supcooking.R;

public class RegisterUserActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText usernameET,nameET,lastnameET,emailET,passwordET,phoneET,addressET;
    private String username,name,lastname,email,password,phone,address;
    private Button RegisterBTN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);

        //EditText
        usernameET = findViewById(R.id.username);
        nameET = findViewById(R.id.name);
        lastnameET = findViewById(R.id.lastname);
        emailET = findViewById(R.id.email);
        passwordET = findViewById(R.id.password);
        phoneET = findViewById(R.id.phone);
        addressET = findViewById(R.id.address);

        //Button
        RegisterBTN = findViewById(R.id.register_user_button);
        RegisterBTN.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.register_user_button :

                username = usernameET.getText().toString();
                name = nameET.getText().toString();
                lastname = lastnameET.getText().toString();
                email = emailET.getText().toString();
                password = passwordET.getText().toString();
                phone = phoneET.getText().toString();
                address = addressET.getText().toString();
                User user = new User(username,password,name,lastname,email,address,phone);
                if(user.attempToRegister(usernameET,nameET,lastnameET,emailET,passwordET,phoneET,addressET,getApplicationContext()) == false){

                    long id =  MyDataBase.getInstance(this).userDao().insert(user);
                    if(id!=0){
                        Toast.makeText(getApplicationContext(),"User register", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(this, LoginActivity.class));
                    }
                }
                break;
        }
    }
}
