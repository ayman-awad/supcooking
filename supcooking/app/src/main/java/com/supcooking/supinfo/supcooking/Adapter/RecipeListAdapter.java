package com.supcooking.supinfo.supcooking.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.supcooking.supinfo.supcooking.Classes.Recipe;

import java.util.ArrayList;
import java.util.Optional;

public class RecipeListAdapter extends BaseAdapter {

    private ArrayList<Recipe> recipe;
    private Context context;
    private LayoutInflater inflater;

    public RecipeListAdapter(Context context, ArrayList<Recipe> data){
        this.recipe = data;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return recipe.size();
    }

    @Override
    public Object getItem(int position) {
        return recipe.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view == null){
            view = inflater.inflate(android.R.layout.simple_list_item_1,null);
        }
        Recipe myRecipe = (Recipe) getItem(i);
        TextView tv = view.findViewById(android.R.id.text1);
        tv.setText(myRecipe.getName());
        return view;
    }

}
