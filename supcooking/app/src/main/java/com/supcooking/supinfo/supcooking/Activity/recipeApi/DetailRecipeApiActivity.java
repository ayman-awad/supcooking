package com.supcooking.supinfo.supcooking.Activity.recipeApi;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.supcooking.supinfo.supcooking.Classes.Recipe;
import com.supcooking.supinfo.supcooking.Classes.RecipeFromApi;
import com.supcooking.supinfo.supcooking.DataBase.MyDataBase;
import com.supcooking.supinfo.supcooking.R;

public class DetailRecipeApiActivity extends AppCompatActivity {

    private TextView nameTV,typeTV,timecookingTV,timePreparationTV,stepPrepationTV,ingredientTV;
    private ImageView imageRecipeIV;
    private RatingBar markRB;
    long id_recipe;
    private RecipeFromApi detailApiRecipe;
    RecipeFromApi recipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_recipe_api);

        Bundle intent = getIntent().getExtras();
        id_recipe = intent.getLong("id_recipe");

        //TextView
        nameTV = findViewById(R.id.name_recipe_api_textView);
        typeTV = findViewById(R.id.type_recipe_api_textView);
        timecookingTV = findViewById(R.id.time_cooking_recipe_api_textView);
        timePreparationTV = findViewById(R.id.time_preparation_recipe_api_textView);
        stepPrepationTV = findViewById(R.id.steps_preparation_recipe_api_textView);
        markRB = findViewById(R.id.mark_api_ratingBar);
        ingredientTV = findViewById(R.id.ingredients_recipe_api_textView);

        //ImageView
        imageRecipeIV = findViewById(R.id.image_recipe_api_imageView);

        recipe = MyDataBase.getInstance(this).recipeFromApiDAO().getRecipeByIdrecipe(id_recipe);
        detailApiRecipe(id_recipe);
    }

    public void detailApiRecipe(long id_recipe){

        detailApiRecipe = getRecipeElement(id_recipe);
        String name = detailApiRecipe.getName();
        String type = detailApiRecipe.getType();
        String timeCook = String.valueOf(detailApiRecipe.getTimeCooking());
        String timePreparation = String.valueOf(detailApiRecipe.getTimePreparation());
        String steps = String.valueOf(detailApiRecipe.getStepsPreparation());
        String ingredient = String.valueOf(detailApiRecipe.getIngredient());

        nameTV.setText(name);
        typeTV.setText(type);
        timecookingTV.setText(timeCook);
        timePreparationTV.setText(timePreparation);
        stepPrepationTV.setText(steps);
        ingredientTV.setText(ingredient);

        Picasso.get().load(detailApiRecipe.getPathImg()).into(imageRecipeIV);

    }

    private RecipeFromApi getRecipeElement(long idrecipe){
        RecipeFromApi myRecipe = MyDataBase.getInstance(this).recipeFromApiDAO().getRecipeByIdrecipe(idrecipe);
        return myRecipe;
    }
}
