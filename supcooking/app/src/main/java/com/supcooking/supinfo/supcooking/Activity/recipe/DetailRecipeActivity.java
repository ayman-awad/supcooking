package com.supcooking.supinfo.supcooking.Activity.recipe;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.supcooking.supinfo.supcooking.Activity.user.HomeUserActivity;
import com.supcooking.supinfo.supcooking.Classes.Mark;
import com.supcooking.supinfo.supcooking.Classes.Recipe;
import com.supcooking.supinfo.supcooking.Classes.RecipeFromApi;
import com.supcooking.supinfo.supcooking.DataBase.MyDataBase;
import com.supcooking.supinfo.supcooking.R;

import java.util.ArrayList;

public class DetailRecipeActivity extends AppCompatActivity implements View.OnClickListener{

    private long id_user,id_recipe;
    private TextView nameTV,typeTV,timecookingTV,timePreparationTV,stepPrepationTV,ingredientTV;
    private ImageView imageRecipeIV;
    private RatingBar markRB;
    private  Recipe detailRecipe;
    private Button addMark,cancel;
    private Intent i;
    private Context self=this;

    private String urlDefaultImg = "https://storenotrefamilleprod.blob.core.windows.net/images/cms/nouveausite_nf/ressources/images/propositionrecette.png";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_recipe);

        Bundle intent = getIntent().getExtras();
        id_user = intent.getLong("idUser");
        id_recipe = intent.getLong("id_recipe");

        //TextView
        nameTV = findViewById(R.id.name_recipe_textView);
        typeTV = findViewById(R.id.type_recipe_textView);
        timecookingTV = findViewById(R.id.time_cooking_recipe_textView);
        timePreparationTV = findViewById(R.id.time_preparation_recipe_textView);
        stepPrepationTV = findViewById(R.id.steps_preparation_recipe_textView);
        markRB = findViewById(R.id.mark_ratingBar);
        ingredientTV = findViewById(R.id.ingredients_recipe_textView);
        //ImageView
        imageRecipeIV = findViewById(R.id.image_recipe_imageView);

        //Button
        addMark = findViewById(R.id.submit_mark_button);
        cancel = findViewById(R.id.cancel_detail_recipe_button);
        cancel.setOnClickListener(this);
        addMark.setOnClickListener(this);

        seeDetailRecipe();

    }

    /****
     * See all detail fron one recipe
     */
    public void seeDetailRecipe(){

        detailRecipe = getRecipeElement(id_recipe);
        String name = detailRecipe.getName();
        String type = detailRecipe.getType();
        String timeCook = String.valueOf(detailRecipe.getTimeCooking());
        String timePreparation = String.valueOf(detailRecipe.getTimePreparation());
        String steps = String.valueOf(detailRecipe.getStepsPreparation());
        String ingredient = String.valueOf(detailRecipe.getIngredient());

        if (getMark(id_recipe, id_user) == null) {
            markRB.setRating(0);
        }
        else {
            float markRatingBar =  getMark(id_recipe,id_user).getUserMark();
            markRB.setRating(markRatingBar);
        }

        nameTV.setText(name);
        typeTV.setText(type);
        timecookingTV.setText(timeCook);
        timePreparationTV.setText(timePreparation);
        stepPrepationTV.setText(steps);
        ingredientTV.setText(ingredient);
        if(detailRecipe.getPathImg() == null){
            Picasso.get().load(urlDefaultImg).into(imageRecipeIV);

        }else{
            imageRecipeIV.setImageURI(Uri.parse(detailRecipe.getPathImg()));
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            //submit a mark
            case R.id.submit_mark_button :
                float mark = markRB.getRating();
                if(getMark(id_recipe,id_user) == null && mark == 0){
                    createMark(mark);

                }else if (getMark(id_recipe,id_user) != null && mark != 0){
                    updateMark(mark);
                }
                else if (getMark(id_recipe,id_user) == null && mark != 0){
                    createMark(mark);
                }
                returnSeeAllRecipesActivity();
                break;

                //send to see all recipe
            case R.id.cancel_detail_recipe_button :
                returnSeeAllRecipesActivity();
                break;

            case KeyEvent.KEYCODE_BACK :
                goSeeAllRecipesActivity();
                break;
        }
    }


    //give all information from a recipe
    private Recipe getRecipeElement(long idrecipe){
        Recipe myRecipe = MyDataBase.getInstance(self).recipeDao().getRecipeById(idrecipe);
        return myRecipe;
    }

    /**
     * sends to all recipes
     */
    private void returnSeeAllRecipesActivity(){
        i = new Intent(DetailRecipeActivity.this, SeeAllRecipeActivity.class);
        i.putExtra("idUser",id_user);
        startActivity(i);
    }

    private Mark getMark(long idRecipe, long idUser){
        Mark markRecipe = MyDataBase.getInstance(self).markDAO().getMarkuser(idRecipe,idUser);
        return markRecipe;
    }

    /**
     * allows you to create a note.
     * by default to create a recipe notes are zero
     * @param mark
     */
    private void createMark(float mark){
        Mark createMark = new Mark();
        createMark.setUserMark(mark);
        createMark.setFkIdRecipe(id_recipe);
        createMark.setFkuserId(id_user);
        MyDataBase.getInstance(self).markDAO().insert(createMark);
        Toast.makeText(getApplicationContext(),"MARK CREATE", Toast.LENGTH_LONG).show();
    }

    /**
     * allows to modify the given note
     * @param mark
     */
    private void updateMark(float mark){
        Mark updateMark = getMark(id_recipe,id_user);
        updateMark.setUserMark(mark);
        MyDataBase.getInstance(self).markDAO().update(updateMark);
        Toast.makeText(getApplicationContext(),"MARK UPDATE", Toast.LENGTH_LONG).show();
    }

    private void goSeeAllRecipesActivity(){
        i = new Intent(DetailRecipeActivity.this, SeeAllRecipeActivity.class);
        i.putExtra("idUser",id_user);
        startActivity(i);
    }


}

