package com.supcooking.supinfo.supcooking.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.supcooking.supinfo.supcooking.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
