package com.supcooking.supinfo.supcooking.Classes;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;
import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "ingredients_recipe",
        foreignKeys = @ForeignKey(entity = Recipe.class,
                parentColumns = "id_recipe",
                childColumns = "fk_id_recipe",
                onDelete = CASCADE),
        indices= @Index(value="fk_id_recipe"),
        primaryKeys={"fk_id_ingredient","fk_id_recipe"}
)


public class IngredientInRecipe {

    @NonNull
    @ColumnInfo(name = "fk_id_ingredient")
    private long fkIdIngredient;

    @NonNull
    @ColumnInfo(name = "fk_id_recipe")
    private long fkIdRecipe;

    public IngredientInRecipe(long fk_id_ingredient,long fk_id_recipe ){
            this.fkIdIngredient =fk_id_ingredient;
            this.fkIdRecipe = fk_id_recipe;
    }

    public IngredientInRecipe(){ super ();}

    @NonNull
    public long getFkIdIngredient() {
        return fkIdIngredient;
    }

    public void setFkIdIngredient(@NonNull long fkIdIngredient) {
        this.fkIdIngredient = fkIdIngredient;
    }

    @NonNull
    public long getFkIdRecipe() {
        return fkIdRecipe;
    }

    public void setFkIdRecipe(@NonNull long fkIdRecipe) {
        this.fkIdRecipe = fkIdRecipe;
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
