package com.supcooking.supinfo.supcooking.Classes;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.supcooking.supinfo.supcooking.Database.MyDataBase;
import java.util.ArrayList;
import java.util.List;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "ingredients" ,
        foreignKeys = {
                @ForeignKey(entity = User.class,
                        parentColumns = "id_user",
                        childColumns = "fk_id_user",
                        onDelete = CASCADE)},
        indices = {
                @Index(value="fk_id_user"),
        })

public class Ingredient {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_ingredient")
    private long idIngredients;

    @ColumnInfo(name = "name_ingredient")
    private String nameIngredient;

    @ColumnInfo(name = "fk_id_user")
    private long fkIdUser;

    public Ingredient(){ super();}

    public Ingredient(long id_ingredient,long fk_id_user, String name_ingredient){
        this.idIngredients = id_ingredient;
        this.fkIdUser = fk_id_user;
        this.nameIngredient = name_ingredient;
    }

    @NonNull
    public long getIdIngredients() {
        return idIngredients;
    }

    public void setIdIngredients(@NonNull long idIngredients) {
        this.idIngredients = idIngredients;
    }

    public String getNameIngredient() {
        return nameIngredient;
    }

    public void setNameIngredient(String nameIngredient) {
        this.nameIngredient = nameIngredient;
    }

    public long getFkIdUser() {
        return fkIdUser;
    }

    public void setFkIdUser(long fkIdUser) {
        this.fkIdUser = fkIdUser;
    }

    @Override
    public String toString(){
        return nameIngredient;
    }


}


