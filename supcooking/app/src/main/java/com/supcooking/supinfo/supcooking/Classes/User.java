package com.supcooking.supinfo.supcooking.Classes;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;


import com.supcooking.supinfo.supcooking.R;

import java.io.Serializable;


@Entity(tableName = "users")
public class User implements Serializable {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_user")
    private long idUser;

    @ColumnInfo(name = "username")
    private String userName;

    @ColumnInfo(name = "password")
    private String password;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "lastname")
    private String lastName;

    @ColumnInfo(name = "email")
    private String email;

    @ColumnInfo(name = "address")
    private String address;

    @ColumnInfo(name = "phone")
    private String phone;

    public User(){ super();}

    public User(String username,String passwordUser,String nameUser, String lastnameUser,String emailUser,String addressUser, String phoneUser){
        this.userName = username;
        this.password = passwordUser;
        this.name = nameUser;
        this.lastName = lastnameUser;
        this.email = emailUser;
        this.phone = phoneUser;
        this.setAddress(addressUser);
    }

    @NonNull
    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(@NonNull long idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    @Override
    public String toString() {
        return getUserName();
    }


    public boolean attempToRegister(EditText usernameET,
                                    EditText nameET,
                                    EditText lastnameET,
                                    EditText emailET,
                                    EditText passwordET,
                                    EditText phoneET,
                                    EditText addressET,
                                    Context context){


        // Reset errors.
        emailET.setError(null);
        passwordET.setError(null);
        usernameET.setError(null);
        phoneET.setError(null);
        nameET.setError(null);
        lastnameET.setError(null);
        addressET.setError(null);

        // Store values at the time of the login attempt.
        String email = emailET.getText().toString();
        String password = passwordET.getText().toString();
        String username = usernameET.getText().toString();
        String name = nameET.getText().toString();
        String lastname = lastnameET.getText().toString();
        String phone = phoneET.getText().toString();
        String address = addressET.getText().toString();

        boolean cancel = false;
        View focusView = null;
        boolean findError = false;


        // Check for a valid password
        if (TextUtils.isEmpty(password)) {
            passwordET.setError(context.getString(R.string.error_field_required));
            focusView = passwordET;
            cancel = true;
            findError = true;
        } else if (!isLengthValid(password)) {
            passwordET.setError(context.getString(R.string.error_invalid_password));
            focusView = passwordET;
            cancel = true;
            findError = true;
        }

        // Check for a valid email address.
       if (TextUtils.isEmpty(email)) {
            emailET.setError(context.getString(R.string.error_field_required));
            focusView = emailET;
            cancel = true;
            findError = false;
        } else if (!isEmailValid(email)) {
            emailET.setError(context.getString(R.string.error_invalid_email));
            focusView = emailET;
            cancel = true;
            findError = true;
        }

        // Check for a valid username
        if (TextUtils.isEmpty(username)) {
            usernameET.setError(context.getString(R.string.error_field_required));
            focusView = usernameET;
            cancel = true;
            findError = true;
        } else if (!isLengthValid(username)) {
            usernameET.setError(context.getString((R.string.error_invalid_length)));
            focusView = usernameET;
            cancel = true;
            findError = true;
        }

        // Check for a valid name
        if (TextUtils.isEmpty(name)) {
            nameET.setError(context.getString(R.string.error_field_required));
            focusView = nameET;
            cancel = true;
            findError = true;
        } else if (!isLengthValid(name)) {
            nameET.setError(context.getString(R.string.error_invalid_length));
            focusView = nameET;
            cancel = true;
            findError = true;
        }

        // Check for a valid lastname
        if (TextUtils.isEmpty(lastname)) {
            lastnameET.setError(context.getString(R.string.error_field_required));
            focusView = lastnameET;
            cancel = true;
            findError = true;
        } else if (!isLengthValid(lastname)) {
            lastnameET.setError(context.getString(R.string.error_invalid_length));
            focusView = lastnameET;
            cancel = true;
            findError = true;
        }

        // Check for a valid phone
        if (TextUtils.isEmpty(phone)) {
            phoneET.setError(context.getString(R.string.error_field_required));
            focusView = phoneET;
            cancel = true;
            findError = true;
        } else if (!isLengthPhoneValid(phone)) {
            phoneET.setError(context.getString(R.string.error_invalid_phone));
            focusView = phoneET;
            cancel = true;
            findError = true;
        }

        if (TextUtils.isEmpty(address)) {
            addressET.setError(context.getString(R.string.error_field_required));
            focusView = addressET;
            cancel = true;
            findError = true;}

        if (cancel) {
            focusView.requestFocus();

        }

        return findError;
    }



    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isLengthValid(String item) {
        return item.length() > 2 && item.length() <= 30 ;
    }


    private boolean isLengthPhoneValid(String phone) {
        return phone.length() > 9;
    }


}
