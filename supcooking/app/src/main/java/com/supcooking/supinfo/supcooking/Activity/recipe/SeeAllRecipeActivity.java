package com.supcooking.supinfo.supcooking.Activity.recipe;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.supcooking.supinfo.supcooking.Activity.user.HomeUserActivity;
import com.supcooking.supinfo.supcooking.Adapter.RecipeListAdapter;
import com.supcooking.supinfo.supcooking.Classes.Recipe;
import com.supcooking.supinfo.supcooking.Classes.RecipeFromApi;
import com.supcooking.supinfo.supcooking.DataBase.MyDataBase;
import com.supcooking.supinfo.supcooking.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SeeAllRecipeActivity extends AppCompatActivity implements View.OnClickListener {
    private Intent i;
    private ListView allRecipelistView;
    private ArrayList<Recipe> allRecipeList = new ArrayList<Recipe>();
    private long id_user,id_recipe;
    private SearchView searchRecipe;
    private Context self = this;
    private RecipeListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_all_recipe);

        allRecipelistView = findViewById(R.id.all_recipe_list_view);

        RecipeListAdapter adapter = new RecipeListAdapter(self,allRecipeList);
        allRecipelistView.setAdapter(adapter);

        Bundle intent = getIntent().getExtras();
        id_user = intent.getLong("idUser");

        List<Recipe> resultQuerylistRecipes = MyDataBase.getInstance(this).recipeDao().getAllRecipe(id_user);
        recipeListView(resultQuerylistRecipes);


        searchRecipe = (SearchView) findViewById(R.id.search_recipe_search_view);

        seeOneRecipeActivity();
        searchRecipe();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case KeyEvent.KEYCODE_BACK :
                goHomeActivity();
                break;
        }
    }



    //add all recipe in arraylist
    private void recipeListView(List<Recipe> listRecipes) {
        for (Recipe recipe : listRecipes) {
            allRecipeList.add(recipe);
        }
    }

    //on click send user on detail activity
    private void seeOneRecipeActivity(){
        allRecipelistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                id_recipe = (int) allRecipeList.get(position).getIdRecipe();
                goOnDetailActivity();
            }
        });
    }

    //TODO : doesn't work
    private void searchRecipe(){
        searchRecipe.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(allRecipeList.contains(query)){
                    allRecipelistView.setFilterText(query);
                }else{
                    Toast.makeText(self, "No Match found", Toast.LENGTH_LONG).show();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }

        });
    }

    //send to the detail recipe
    private void goOnDetailActivity(){
        i = new Intent(self, DetailRecipeActivity.class);
        i.putExtra("idUser",id_user);
        i.putExtra("id_recipe",id_recipe);
        startActivity(i);
    }

    private void goHomeActivity(){
        i = new Intent(self, HomeUserActivity.class);
        i.putExtra("idUser",id_user);
        startActivity(i);
    }

}
