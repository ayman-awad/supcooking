package com.supcooking.supinfo.supcooking.JsonParse;


import android.os.AsyncTask;

import com.supcooking.supinfo.supcooking.Activity.LoginActivity;
import com.supcooking.supinfo.supcooking.Activity.recipeApi.RecipeFromApiActivity;
import com.supcooking.supinfo.supcooking.Classes.RecipeFromApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


public class MyJsonParse extends AsyncTask<Void, Void, Void> {

    String userAPI = "http://supinfo.steve-colinet.fr/supcooking/?action=login&login=admin&password=admin";
    String recipeAPI = "http://supinfo.steve-colinet.fr/supcooking/?action=getCooking&login=admin&password=admin";
    JSONObject JOUserSuccess, JOUser;
    JSONObject JORecipeApi,JORecipe;
    String dataUser = "", dataRecipe="";
    String username,pwd,phoneNumber,lastName,firstName,address,email;
    String userData,successUserValue ;
    String recipeData,successRecipeValue ;


    @Override
    protected Void doInBackground(Void... avoid) {

        //get user from api
        try {
            URL url = new URL(userAPI);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputSream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputSream));

            String line = "";
            while (line != null) {
                line = bufferedReader.readLine();
                dataUser = dataUser + line;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //get recipes from api
        try {
            URL url = new URL(recipeAPI);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputSream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputSream));

            String line = "";
            while (line != null) {
                line = bufferedReader.readLine();
                dataRecipe = dataRecipe + line;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    protected void onPostExecute(Void avoid) {
        super.onPostExecute(avoid);

        //create user with result api
        try {

            JOUserSuccess = new JSONObject(dataUser);
            JOUser = new JSONObject(dataUser);
            successUserValue  = JOUserSuccess.getString("success");
            userData = JOUser.getString("user");

            if(successUserValue == "true"){
                JSONObject JOUserDate = new JSONObject(userData);
                username = (String) JOUserDate.get("username");
                pwd = (String) JOUserDate.get("password");
                phoneNumber = (String) JOUserDate.get("phoneNumber");
                lastName = (String) JOUserDate.get("lastName");
                firstName = (String) JOUserDate.get("firstName");
                address = (String) JOUserDate.get("address");
                email = (String) JOUserDate.get("email");
            }

            LoginActivity.userAPI.setPhone(phoneNumber);
            LoginActivity.userAPI.setEmail(email);
            LoginActivity.userAPI.setPassword(pwd);
            LoginActivity.userAPI.setLastName(lastName);
            LoginActivity.userAPI.setName(firstName);
            LoginActivity.userAPI.setAddress(address);
            LoginActivity.userAPI.setEmail(email);
            LoginActivity.userAPI.setUserName(username);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //create recipe with result api
        try {
            JORecipeApi= new JSONObject(dataRecipe);
            successRecipeValue  = JORecipeApi.getString("success");
            recipeData = JORecipeApi.getString("recipes");


            JSONArray JA = new JSONArray(recipeData);
            if(successRecipeValue == "true"){

                for(int i=0; i < JA.length() ; i++){
                JORecipe = (JSONObject) JA.get(i);
                   long id = Long.parseLong(String.valueOf(JORecipe.get("id")));
                    String name = String.valueOf(JORecipe.get("name"));
                    String type = String.valueOf(JORecipe.get("type"));
                    float timeCook = Float.parseFloat(String.valueOf(JORecipe.get("cookingTime")));
                    float preparationtTime = Float.parseFloat(String.valueOf(JORecipe.get("preparationtTime")));
                    String preparationSteps = String.valueOf(JORecipe.get("preparationSteps"));
                    String ingredients = String.valueOf(JORecipe.get("ingredients"));
                    String picture = String.valueOf(JORecipe.get("picture"));

                    RecipeFromApi rfa = new RecipeFromApi();
                    rfa.setIdRecipe(id);
                    rfa.setName(name);
                    rfa.setType(type);
                    rfa.setTimeCooking(timeCook);
                    rfa.setTimePreparation(preparationtTime);
                    rfa.setStepsPreparation(preparationSteps);
                    rfa.setIngredient(ingredients);
                    rfa.setPathImg(picture);

                    LoginActivity.rfa.add(rfa);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


}






