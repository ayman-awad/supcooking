package com.supcooking.supinfo.supcooking.DataBase;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.supcooking.supinfo.supcooking.Classes.Mark;
import com.supcooking.supinfo.supcooking.Classes.Recipe;
import com.supcooking.supinfo.supcooking.Classes.RecipeFromApi;
import com.supcooking.supinfo.supcooking.Classes.User;
import com.supcooking.supinfo.supcooking.DataBase.Dao.MarkDao;
import com.supcooking.supinfo.supcooking.DataBase.Dao.RecipeDao;
import com.supcooking.supinfo.supcooking.DataBase.Dao.RecipeFromApiDao;
import com.supcooking.supinfo.supcooking.DataBase.Dao.UserDao;

import static com.supcooking.supinfo.supcooking.DataBase.MyDataBase.DATA_BASE_VERSION;

@Database(entities = {
        User.class,
        Recipe.class,
        Mark.class,
        RecipeFromApi.class,},
        version = DATA_BASE_VERSION,
        exportSchema = false)

public abstract class MyDataBase extends RoomDatabase {
    public static final int DATA_BASE_VERSION = 1;
    public static final String DATA_BASE_NAME = "SupCookingDataBase";

    public abstract UserDao userDao();
    public abstract RecipeDao recipeDao();
    public abstract MarkDao markDAO();
    public abstract RecipeFromApiDao recipeFromApiDAO();
    public static MyDataBase INSTANCE;

    public static MyDataBase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(
                    context.getApplicationContext(),
                    MyDataBase.class,
                    DATA_BASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }

    public static void DESTROY_INSTANCE() {
        INSTANCE = null;
    }

}
