package com.supcooking.supinfo.supcooking.Activity.recipeApi;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.supcooking.supinfo.supcooking.Activity.LoginActivity;
import com.supcooking.supinfo.supcooking.Activity.recipe.DetailRecipeActivity;
import com.supcooking.supinfo.supcooking.Activity.recipe.SeeAllRecipeActivity;
import com.supcooking.supinfo.supcooking.Classes.RecipeFromApi;
import com.supcooking.supinfo.supcooking.DataBase.MyDataBase;
import com.supcooking.supinfo.supcooking.JsonParse.MyJsonParse;
import com.supcooking.supinfo.supcooking.R;

import java.util.ArrayList;
import java.util.List;

public class RecipeFromApiActivity extends AppCompatActivity {

    private ArrayList<RecipeFromApi> allRecipeFromApi = new ArrayList<RecipeFromApi>();
    private ListView allRecipeApiListView;
    private Intent i;
    private long id_recipe;
    private Context self = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_from_api);

        allRecipeApiListView = findViewById(R.id.all_recipe_api_list_view);
        ArrayAdapter<RecipeFromApi> adapterRecipeApi = new ArrayAdapter<RecipeFromApi>(this,android.R.layout.simple_list_item_1, allRecipeFromApi);
        allRecipeApiListView.setAdapter(adapterRecipeApi);

        List<RecipeFromApi> resultQuerylistRecipesApi = MyDataBase.getInstance(this).recipeFromApiDAO().getAll();
        recipeApiListView(resultQuerylistRecipesApi);


        seeOneRecipeApiActivity();
    }

    private void recipeApiListView(List<RecipeFromApi> listRecipes) {
        for (RecipeFromApi recipe : listRecipes) {
            allRecipeFromApi.add(recipe);
        }
    }

    /**
     *send to the detail of the recipe after a click on this one
     */
    private void seeOneRecipeApiActivity(){
        allRecipeApiListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                id_recipe = (int) allRecipeFromApi.get(position).getIdRecipe();
                i = new Intent(self, DetailRecipeApiActivity.class);
                i.putExtra("id_recipe",id_recipe);
                startActivity(i);
            }
        });
    }




}
