package com.supcooking.supinfo.supcooking.DataBase.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.supcooking.supinfo.supcooking.Classes.RecipeFromApi;

import java.util.List;

@Dao
public interface RecipeFromApiDao {

    @Query("SELECT * FROM recipeFromApi ORDER BY id_recipe desc")
    List<RecipeFromApi> getAll();

    @Insert(onConflict = OnConflictStrategy.ABORT)
    long insert(RecipeFromApi  rfa);

    @Delete
    void delete(RecipeFromApi...  rfa);

    @Update
    void update(RecipeFromApi...  rfa);

    @Query("SELECT * FROM RecipeFromApi WHERE id_recipe=:id")
    RecipeFromApi getRecipeByIdrecipe(long id);
}
