package com.supcooking.supinfo.supcooking.Classes;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "marks",foreignKeys = {
        @ForeignKey(entity = User.class,
                parentColumns = "id_user",
                childColumns = "fk_id_user",
                onDelete = CASCADE),
        @ForeignKey(entity = Recipe.class,
                parentColumns="id_recipe",
                childColumns="fk_id_recipe",
                onDelete=CASCADE)},
        indices= {
                @Index(value="fk_id_user"),
                @Index(value="fk_id_recipe")
        })

public class Mark {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_mark")
    private long idMark;

    @ColumnInfo(name = "user_mark")
    private float userMark;

    @NonNull
    @ColumnInfo(name = "fk_id_recipe")
    private long fkIdRecipe;

    @NonNull
    @ColumnInfo(name = "fk_id_user")
    private long fkuserId;

    public Mark(){
        super();
    }

    public Mark(long id_mark,float user_mark,long fk_idRecipe,long fk_user_id,float markUser){
        this.idMark = id_mark;
        this.userMark = user_mark;
        this.fkIdRecipe = fk_idRecipe;
        this.fkuserId = fk_user_id;
    }

    @NonNull
    public long getIdMark() {
        return idMark;
    }

    public void setIdMark(@NonNull long idMark) {
        this.idMark = idMark;
    }

    public float getUserMark() {
        return userMark;
    }

    public void setUserMark(float userMark) {
        this.userMark = userMark;
    }

    @NonNull
    public long getFkIdRecipe() {
        return fkIdRecipe;
    }

    public void setFkIdRecipe(@NonNull long fkIdRecipe) {
        this.fkIdRecipe = fkIdRecipe;
    }

    @NonNull
    public long getFkuserId() {
        return fkuserId;
    }

    public void setFkuserId(@NonNull long fkuserId) {
        this.fkuserId = fkuserId;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
