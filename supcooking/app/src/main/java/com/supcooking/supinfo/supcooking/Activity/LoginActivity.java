package com.supcooking.supinfo.supcooking.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.supcooking.supinfo.supcooking.Activity.user.RegisterUserActivity;
import com.supcooking.supinfo.supcooking.Activity.user.HomeUserActivity;
import com.supcooking.supinfo.supcooking.Classes.RecipeFromApi;
import com.supcooking.supinfo.supcooking.Classes.User;
import com.supcooking.supinfo.supcooking.DataBase.MyDataBase;
import com.supcooking.supinfo.supcooking.JsonParse.MyJsonParse;
import com.supcooking.supinfo.supcooking.R;

import java.util.ArrayList;
import java.util.List;



public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static int LOGIN_ACTIVITY_CODE = 1;
    public static List<RecipeFromApi> rfa = new ArrayList<>();
    private  EditText usernameET,passwordET;
    private Button btnRegister,btnLoginApi;
    public static User userAPI = new User();
    private Context self = this;
    private String username, pwd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //EditText
        usernameET = findViewById(R.id.username);
        passwordET = (EditText) findViewById(R.id.password);

        //Button ans listener
        btnRegister = findViewById(R.id.register_button);
        btnRegister.setOnClickListener(this);
        Button mEmailSignInButton = (Button) findViewById(R.id.sign_in_button);
        mEmailSignInButton.setOnClickListener(this);
        btnLoginApi = findViewById(R.id.login_in_api_btn);
        btnLoginApi.setOnClickListener(this);

        //execute the json parse
        MyJsonParse mjp = new MyJsonParse();
        mjp.execute();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            //check if user exist and send him to home user activity
            case R.id.sign_in_button:

                username = usernameET.getText().toString();
                pwd = passwordET.getText().toString();

                long id = MyDataBase.getInstance(self).userDao().userExist(username, pwd);
                if(!username.isEmpty() && !pwd.isEmpty()){
                    if(id != 0 ){
                        goHomeActivity(id);
                    }
                    else{
                        Toast.makeText(getApplicationContext(),"Error on email or password", Toast.LENGTH_SHORT).show();
                    }
                }
                seeRecipeFromApi(rfa);
                    break;

            //send to register activity to create a user
            case R.id.register_button:
                startActivity(new Intent(self, RegisterUserActivity.class));
                break;

                //if use want to log with a api user
            case R.id.login_in_api_btn :

                username = usernameET.getText().toString();
                pwd = passwordET.getText().toString();

                long userExistInDB = MyDataBase.getInstance(self).userDao().userExist(username,pwd);

                if(userAPI != null && userExistInDB == 0){
                    if(!username.isEmpty() && !pwd.isEmpty()){
                        long adduserApiInDB = MyDataBase.getInstance(self).userDao().insert(userAPI);
                        goHomeActivity(adduserApiInDB);
                    }

                }else if (userExistInDB != 0){
                    if(username.equals(userAPI.getUserName()) &&  pwd.equals(userAPI.getPassword()))  {
                      goHomeActivity(userExistInDB);
                    }
                }
                seeRecipeFromApi(rfa);
                break;
        }

    }

    public void onBackPressed() {
        super.onStop();
    }

    /**
     * send to home activity user
     * @param id_user
     */
    private void goHomeActivity(long id_user){
        Intent i = new Intent(self, HomeUserActivity.class);
        i.putExtra("idUser", id_user);
        startActivityForResult(i,LOGIN_ACTIVITY_CODE);
    }

    /**
     * stock all API recipes if there is no network
     * @param list (list of all recipe from the api)
     */
    private void seeRecipeFromApi(List<RecipeFromApi> list){
        if(MyDataBase.getInstance(self).recipeFromApiDAO().getAll().isEmpty()){
            if(!rfa.isEmpty()){
                for (RecipeFromApi oneRecipe:list) {
                    MyDataBase.getInstance(self).recipeFromApiDAO().insert(oneRecipe);
                }
            }
        }
    }



}
