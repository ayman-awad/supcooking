package com.supcooking.supinfo.supcooking.Activity.recipe;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.supcooking.supinfo.supcooking.Activity.user.HomeUserActivity;
import com.supcooking.supinfo.supcooking.Classes.Recipe;
import com.supcooking.supinfo.supcooking.DataBase.MyDataBase;
import com.supcooking.supinfo.supcooking.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class RegisterRecipeActivity extends AppCompatActivity implements View.OnClickListener{

    private static final int REQUEST_TAKE_PHOTO = 1;

    private long idUser,insertRecipe;
    private EditText nameET,typeET,timeCookingET,timePreparationET,stepPreparationET,ingredientET;
    private String name,type,img,stepPreparation,ingredient;
    private  Integer timeCooking, timePreparation;
    private Button register_recipe,cancel,takePictureButton;
    private ImageView imageRecipe;
    private Context self = this;
    private String mCurrentPhotoPath;
    private String urlDefaultImg = "https://grimpavranches.com/wp-content/uploads/2018/01/symbole-repas-orange.gif";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_recipe);

        //Bundle
        Bundle intent = getIntent().getExtras();
        idUser = intent.getLong("idUser");

        // EDIT TEXT
        nameET = findViewById(R.id.name_recipe_editText);
        typeET = findViewById(R.id.type_recipe_editText);
        timeCookingET = findViewById(R.id.time_cooking_editText);
        timePreparationET = findViewById(R.id.time_preparation_editText);
        stepPreparationET = findViewById(R.id.steps_preparation_editText);
        ingredientET = findViewById(R.id.ingredients_editText);

        //ImageView
        imageRecipe = findViewById(R.id.image_path_imageView);
        Picasso.get().load(urlDefaultImg).into(imageRecipe);
        //BUTTON
        register_recipe = findViewById(R.id.register_recipe_button);
        takePictureButton = (Button) findViewById(R.id.button_image);
        cancel = findViewById(R.id.register_recipe_cancel_button);

        //Set listener
        cancel.setOnClickListener(this);
        register_recipe.setOnClickListener(this);
        takePictureButton.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            //button to register recipe
            case R.id.register_recipe_button :
                if(attempToregisterRecipe() == false){
                    //EditText in string
                    name = nameET.getText().toString();
                    type = typeET.getText().toString();
                    timeCooking = Integer.valueOf(timeCookingET.getText().toString());
                    timePreparation = Integer.valueOf(timePreparationET.getText().toString());
                    stepPreparation = stepPreparationET.getText().toString();
                    ingredient = ingredientET.getText().toString();

                    Recipe recipe = new Recipe();

                    if(mCurrentPhotoPath == null){
                        mCurrentPhotoPath = urlDefaultImg;
                        img = mCurrentPhotoPath;
                    }else {
                        img = mCurrentPhotoPath;
                    }

                    recipe = new Recipe(name,type,timeCooking,timePreparation,stepPreparation,ingredient,img,idUser);
                    insertRecipe =  MyDataBase.getInstance(this).recipeDao().insert(recipe);


                    if(insertRecipe!= 0){
                        Toast.makeText(getApplicationContext(),"Recipe Register", Toast.LENGTH_LONG).show();
                        Intent i = new Intent(this, HomeUserActivity.class);
                        i.putExtra("idUser", idUser);
                        startActivity(i);
                    }
                } else{
                    Toast.makeText(getApplicationContext(),"Some fields are empty", Toast.LENGTH_LONG).show();
                }

                break;

            //cancel register reicpe
            case R.id.register_recipe_cancel_button :
                Intent i = new Intent(this, HomeUserActivity.class);
                i.putExtra("idUser", idUser);
                startActivity(i);

                break;

                // button take a picture
            case R.id.button_image :
                dispatchTakePictureIntent();

                break;
        }
    }

    /**
     *create a image file
     * and send the image in PictureSupCooking folder
     * @return image
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES+"/PictureSupCooking/");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    /**
     * Start the ACTION_IMAGE_CAPTURE to take picture
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }


    /**
     *
     * @return findError value / false is not error are find
     */
    private Boolean attempToregisterRecipe(){

        // Reset errors.
        nameET.setError(null);
        typeET.setError(null);
        timeCookingET.setError(null);
        timePreparationET.setError(null);
        stepPreparationET.setError(null);

        // Store values at the time of the login attempt.
        String name = nameET.getText().toString();
        String type = typeET.getText().toString();
        String timeCook = timeCookingET.getText().toString();
        String timePreparation = nameET.getText().toString();
        String StepPrepa = stepPreparationET.getText().toString();

        boolean cancel = false;
        View focusView = null;
        boolean findError = false;


        // Check all fields are not empty
        if (TextUtils.isEmpty(name)) {
            nameET.setError(getString(R.string.error_field_required));
            focusView = nameET;
            cancel = true;
            findError = true;
        } else if (TextUtils.isEmpty(type)) {
            typeET.setError(getString(R.string.error_field_required));
            focusView = typeET;
            cancel = true;
            findError = true;
        } else if (TextUtils.isEmpty(timeCook)) {
            timeCookingET.setError(getString(R.string.error_field_required));
            focusView = timeCookingET;
            cancel = true;
            findError = true;
        } else if (TextUtils.isEmpty(timePreparation)) {
            timePreparationET.setError(getString(R.string.error_field_required));
            focusView = timePreparationET;
            cancel = true;
            findError = true;
        } else if (TextUtils.isEmpty(StepPrepa)) {
            stepPreparationET.setError(getString(R.string.error_field_required));
            focusView = stepPreparationET;
            cancel = true;
            findError = true;
        }

        if (cancel) {
            focusView.requestFocus();
        }
        return findError;
    }


}
