package com.supcooking.supinfo.supcooking.Activity.user;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.supcooking.supinfo.supcooking.Activity.LoginActivity;
import com.supcooking.supinfo.supcooking.Activity.recipe.DetailRecipeActivity;
import com.supcooking.supinfo.supcooking.Activity.recipe.RegisterRecipeActivity;
import com.supcooking.supinfo.supcooking.Activity.recipeApi.RecipeFromApiActivity;
import com.supcooking.supinfo.supcooking.Activity.recipe.SeeAllRecipeActivity;
import com.supcooking.supinfo.supcooking.Adapter.RecipeListAdapter;
import com.supcooking.supinfo.supcooking.Classes.Recipe;
import com.supcooking.supinfo.supcooking.Classes.User;
import com.supcooking.supinfo.supcooking.DataBase.MyDataBase;
import com.supcooking.supinfo.supcooking.R;

import java.util.ArrayList;
import java.util.List;

import static com.supcooking.supinfo.supcooking.DataBase.MyDataBase.DESTROY_INSTANCE;

public class HomeUserActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener,AdapterView.OnItemLongClickListener {

    private static int HOME_ACTIVITY_CODE = 2;
    private BottomNavigationView navigation;
    private long id_user;
    private Intent i;
    private TextView nameUserTV;
    private ListView allRecipeLV;
    private FloatingActionButton profileFloatingBTN,addRecipe;
    private ArrayList<Recipe> allRecipeList = new ArrayList<Recipe>();
    private Activity self = this;
    RecipeListAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_user);

        //Button
        nameUserTV = findViewById(R.id.name_user_textView);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        //Floating action button
        addRecipe = findViewById(R.id.add_recipe_btn);
        addRecipe.setOnClickListener(this);
        profileFloatingBTN = findViewById(R.id.add_update_profile_floatingButton);
        profileFloatingBTN.setOnClickListener(this);

        //Navigation
        navigation.setOnNavigationItemSelectedListener(this);
        allRecipeLV = findViewById(R.id.recipe_user_list_view);

        Bundle intent = getIntent().getExtras();
        id_user = intent.getLong("idUser");
        User user = MyDataBase.getInstance(this).userDao().getUserById(id_user);
        welcomeUser(user);

        List<Recipe> resultQuerylistRecipes = MyDataBase.getInstance(this).recipeDao().getRecipeByUserId(id_user);
        recipeListView(resultQuerylistRecipes);

        //Adapter recipe
        adapter = new RecipeListAdapter(self,allRecipeList);
        allRecipeLV.setAdapter(adapter);
        allRecipeLV.setOnItemLongClickListener(this);

        goUpdateActivity();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            //update information user
            case R.id.add_update_profile_floatingButton:
                i = new Intent(self, UpdateUserProfileActivity.class);
                i.putExtra("idUser",id_user);
                startActivityForResult(i, HOME_ACTIVITY_CODE);
                break;

                //send to register activity to create a new recipe
            case R.id.add_recipe_btn :
                i = new Intent(this, RegisterRecipeActivity.class);
                i.putExtra("idUser",id_user);
                startActivity(i);
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            //see all recipe from the API
            case R.id.navigation_api_recipe :
                i = new Intent(this, RecipeFromApiActivity.class);
                startActivity(i);
                break;

                //See all recipe from all users
            case R.id.navigation_see_all_recipe :
                i = new Intent(this, SeeAllRecipeActivity.class);
                i.putExtra("idUser",id_user);
                startActivity(i);
                break;

                //redirect on login activity
            case R.id.navigation_logout :
                DESTROY_INSTANCE();
                startActivity(new Intent(this, LoginActivity.class));
                break;
        }
        return true;
    }

    /**
     * get all recipe create by the user
     * add them to the arraylist to show them
     * @param listRecipes
     * @return ArrayList<recipe>
     */
    public List<Recipe> recipeListView(List<Recipe> listRecipes) {
        for (Recipe recipe : listRecipes) {
            allRecipeList.add(recipe);
        }
        return allRecipeList;
    }

    public void welcomeUser(User user) {
        nameUserTV.setText("Welcome " + " " + user.getUserName());
    }

    /**
     * send to the detail of the recipe after a click on this one
     */
    public void goUpdateActivity() {
        allRecipeLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long id_recipe = (long) allRecipeList.get(position).getIdRecipe();
                i = new Intent(self,UpdateRecipeUserActivity.class);
                i.putExtra("id_recipe", id_recipe);
                i.putExtra("idUser", id_user);
                startActivityForResult(i, HOME_ACTIVITY_CODE);
            }
        });
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
        AlertDialog.Builder removeRecipe = new AlertDialog.Builder(this);
        removeRecipe.setMessage("Remove your recipe");
        removeRecipe.setCancelable(true);

        removeRecipe.setPositiveButton(
                "Remove ",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                       long removeRecipe =  (long) allRecipeList.get(position).getIdRecipe();
                       Recipe recipe =  MyDataBase.getInstance(self).recipeDao().getRecipeById(removeRecipe);
                       MyDataBase.getInstance(self).recipeDao().delete(recipe);
                       allRecipeList.remove(position);
                        adapter.notifyDataSetChanged();
                       dialog.cancel();
                    }
                });

        removeRecipe.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = removeRecipe.create();
        alert.show();
        return true;
    }
}
