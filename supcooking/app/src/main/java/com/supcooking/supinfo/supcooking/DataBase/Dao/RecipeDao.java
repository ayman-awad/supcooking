package com.supcooking.supinfo.supcooking.DataBase.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.supcooking.supinfo.supcooking.Classes.Recipe;

import java.util.List;

@Dao
public interface RecipeDao {

    @Query("SELECT * FROM recipes ORDER BY id_recipe desc")
    List<Recipe> getAll();


    @Query("SELECT * FROM recipes WHERE fk_id_user !=:id")
    List<Recipe> getAllRecipe(long id);


    @Query("SELECT * FROM recipes WHERE id_recipe=:id")
    Recipe getRecipeById(long id);

    @Query("SELECT * FROM recipes WHERE fk_id_user=:id")
    List<Recipe> getRecipeByUserId(long id);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    long insert(Recipe  recipe);

    @Delete
    void delete(Recipe...  recipe);

    @Update
    void update(Recipe...  recipe);

}
