package com.supcooking.supinfo.supcooking.Activity.user;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.supcooking.supinfo.supcooking.Classes.Recipe;
import com.supcooking.supinfo.supcooking.DataBase.MyDataBase;
import com.supcooking.supinfo.supcooking.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UpdateRecipeUserActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int REQUEST_TAKE_PHOTO = 1;

    private long id_user,id_recipe;
    private EditText nameET,typeET,timeCookingET,timePreparationET,stepPreparationET,ingredientET;
    private String name,type,img,stepPreparation,ingredient;
    private Integer timeCooking, timePreparation;
    private Button updateRecipeBTN,cancelBTN,takePictureBTN;
    private ImageView imageRecipe;
    private Recipe myRecipe;
    private Context self = this;
    private String mCurrentPhotoPath;
    private Intent i;
    private String urlDefaultImg = "https://storenotrefamilleprod.blob.core.windows.net/images/cms/nouveausite_nf/ressources/images/propositionrecette.png";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_recipe_user);

        Bundle intent = getIntent().getExtras();
        id_user = intent.getLong("idUser");
        id_recipe = intent.getLong("id_recipe");

        myRecipe = MyDataBase.getInstance(self).recipeDao().getRecipeById(id_recipe);

        //EditText
        nameET = findViewById(R.id.update_name_recipe_editText);
        typeET  = findViewById(R.id.update_type_recipe_editText);
        timeCookingET = findViewById(R.id.update_time_cooking_editText);
        timePreparationET = findViewById(R.id.update_time_preparation_editText);
        stepPreparationET = findViewById(R.id.update_steps_preparation_editText);
        ingredientET = findViewById(R.id.update_ingredients_editText);
        imageRecipe = findViewById(R.id.update_image_path_imageView);

        //set the EditTex with all information recipe
        nameET.setText(myRecipe.getName());
        typeET.setText(myRecipe.getType());
        timeCookingET.setText(String.valueOf(myRecipe.getTimeCooking()));
        timePreparationET.setText(String.valueOf(myRecipe.getTimePreparation()));
        stepPreparationET.setText(String.valueOf(myRecipe.getStepsPreparation()));
        ingredientET.setText(myRecipe.getIngredient());

        //Button
        updateRecipeBTN = findViewById(R.id.update_register_recipe_button);
        cancelBTN = findViewById(R.id.update_recipe_cancel_button);
        takePictureBTN  = findViewById(R.id.update_button_image);
        //Button listener
        updateRecipeBTN.setOnClickListener(this);
        cancelBTN.setOnClickListener(this);
        takePictureBTN.setOnClickListener(this);

        if(myRecipe.getPathImg() == null){
            Picasso.get().load(urlDefaultImg).into(imageRecipe);
        }else{
            imageRecipe.setImageURI(Uri.parse(myRecipe.getPathImg()));
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.update_register_recipe_button :
                updateRecipeUser();
                break;

            case R.id.update_recipe_cancel_button : Intent i = new Intent(this, HomeUserActivity.class);
                i.putExtra("idUser", id_user);
                startActivity(i); break;

            case R.id.update_button_image :
                dispatchTakePictureIntent();
                break;
        }
    }

    private void updateRecipeUser(){
        name = nameET.getText().toString();
        type = typeET.getText().toString();
        timeCooking = Integer.valueOf(timeCookingET.getText().toString());
        timePreparation = Integer.valueOf(timePreparationET.getText().toString());
        stepPreparation = stepPreparationET.getText().toString();
        ingredient = ingredientET.getText().toString();

        if(mCurrentPhotoPath != null){
            img = mCurrentPhotoPath;
        }
        else{
            img = myRecipe.getPathImg();
        }

        myRecipe.setName(name);
        myRecipe.setType(type);
        myRecipe.setTimeCooking(timeCooking);
        myRecipe.setTimePreparation(timePreparation);
        myRecipe.setStepsPreparation(stepPreparation);
        myRecipe.setIngredient(ingredient);
        myRecipe.setPathImg(img);

        MyDataBase.getInstance(this).recipeDao().update(myRecipe);

        Toast.makeText(getApplicationContext(),"Recipe update", Toast.LENGTH_LONG).show();
        i = new Intent(self, HomeUserActivity.class);
        i.putExtra("idUser",id_user);
        startActivity(i);

    }


    /**
     *create a image file
     * and send the image in PictureSupCooking folder
     * @return image
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES+"/PictureSupCooking/");
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    /**
     * Start the ACTION_IMAGE_CAPTURE to take picture
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }
}
