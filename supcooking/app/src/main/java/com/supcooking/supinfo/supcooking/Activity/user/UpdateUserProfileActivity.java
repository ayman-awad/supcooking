package com.supcooking.supinfo.supcooking.Activity.user;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.supcooking.supinfo.supcooking.Classes.User;
import com.supcooking.supinfo.supcooking.DataBase.MyDataBase;
import com.supcooking.supinfo.supcooking.R;

import java.util.ArrayList;

public class UpdateUserProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private Intent i;
    private EditText usernameET,nameET,lastnameET,emailET,phoneET,passwordET,addressET;
    private String username,name,lasntname,email,password,phone,address,image;
    private Button cancel,update;
    private long id_user;
    private User user;
    private  Activity self = this;
    private ListView allIngredientLV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user_profile);

        Bundle intent = getIntent().getExtras();
        id_user = intent.getLong("idUser");

        //EditText
        usernameET = findViewById(R.id.username_updateTextView);
        nameET = findViewById(R.id.name_updateTextView);
        lastnameET = findViewById(R.id.lastname_updateTextView);
        emailET = findViewById(R.id.email_updateTextView);
        phoneET = findViewById(R.id.phone_updateTextView);
        passwordET = findViewById(R.id.password_updateTextView);
        addressET = findViewById(R.id.address_updateTextView);

        //Button
        cancel = findViewById(R.id.cancel_update_user_button);
        update = findViewById(R.id.submit_update_user_button);

        cancel.setOnClickListener(this);
        update.setOnClickListener(this);

        user = MyDataBase.getInstance(this).userDao().getUserById(id_user);

        //set the EditTex with all information user
        usernameET.setText(user.getUserName());
        nameET.setText(user.getName());
        lastnameET.setText(user.getLastName());
        emailET.setText(user.getEmail());
        phoneET.setText(user.getPhone());
        passwordET.setText(user.getPassword());
        addressET.setText(user.getAddress());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.cancel_update_user_button :
                Toast.makeText(getApplicationContext(),"UPDATE CANCEL", Toast.LENGTH_LONG).show();
                i = new Intent(self, HomeUserActivity.class);
                i.putExtra("idUser",id_user);
                startActivity(i);
                break;

            case R.id.submit_update_user_button :

                if(user.attempToRegister(
                        usernameET,
                        nameET,
                        lastnameET,
                        emailET,
                        passwordET,
                        phoneET,
                        addressET,
                        getApplicationContext()) == false) {

                    username = usernameET.getText().toString();
                    name = nameET.getText().toString();
                    lasntname = lastnameET.getText().toString();
                    email = emailET.getText().toString();
                    password = passwordET.getText().toString();
                    phone = phoneET.getText().toString();
                    address = addressET.getText().toString();

                    user.setUserName(username);
                    user.setName(name);
                    user.setLastName(lasntname);
                    user.setEmail(email);
                    user.setPassword(password);
                    user.setPhone(phone);
                    user.setAddress(address);

                    MyDataBase.getInstance(this).userDao().update(user);

                    Toast.makeText(getApplicationContext(),"User update", Toast.LENGTH_LONG).show();
                    i = new Intent(self, HomeUserActivity.class);
                    i.putExtra("idUser",id_user);
                    startActivity(i);
                    break;
                }
                else {
                    Toast.makeText(getApplicationContext(),"Please fix the error", Toast.LENGTH_SHORT).show();
                }
        }

    }


}
