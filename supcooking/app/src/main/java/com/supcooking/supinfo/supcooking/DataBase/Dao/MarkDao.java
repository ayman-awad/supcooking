package com.supcooking.supinfo.supcooking.DataBase.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.supcooking.supinfo.supcooking.Classes.Mark;


@Dao
public interface MarkDao {

    @Query("select * from marks where fk_id_recipe=:id_recipe and fk_id_user=:id_user")
    Mark getMarkuser(long id_recipe,long id_user);

    @Insert(onConflict = OnConflictStrategy.ABORT)
    long insert(Mark mark);

    @Delete
    void delete(Mark...  mark);

    @Update
    void update(Mark...  mark);


}

